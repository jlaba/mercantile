Source: mercantile
Section: science
Priority: optional
Maintainer: Debian GIS Project <pkg-grass-devel@lists.alioth.debian.org>
Uploaders: Joachim Langenbach <joachim.langenbach@engsas.de>
Build-Depends: debhelper-compat (=10),
               dh-python,
               python3-all,
               python3-click,
               python3-setuptools,
               python3-pytest,
               python3-pytest-cov,
               python3-pydocstyle,
               python3-hypothesis,
               python3-sphinx,
               python3-numpydoc,
               jdupes,
Standards-Version: 4.5.0
Homepage: https://github.com/mapbox/mercantile
Rules-Requires-Root: no
Testsuite: autopkgtest-pkg-python
Vcs-Browser: https://salsa.debian.org/debian-gis-team/mercantile
Vcs-Git: https://salsa.debian.org/debian-gis-team/mercantile.git

Package: python3-mercantile
Architecture: all
Section: python
Depends: libjs-jquery,
         libjs-underscore,
	 ${python3:Depends},
	 ${misc:Depends},
	 ${sphinxdoc:Depends},
Recommends: ${python3:Recommends}
Suggests: ${python3:Suggests}
Description:  Spherical mercator coordinate and tile utilities
 The mercantile module provides functions that respectively return the upper
 left corner and bounding longitudes and latitudes for XYZ tiles, spherical
 mercator x and y coordinates, tile containing a given point, and quadkey
 conversion functions for translating between quadkey and tile coordinates.
 .
 Also in mercantile are functions to traverse the tile stack.
 Named tuples are used to represent tiles, coordinates, and bounding boxes.

Package: mercantile
Architecture: all
Section: utils
Depends: python3-mercantile (>= ${binary:Version}),
	${python3:Depends},
	${misc:Depends}
Description: Command line utility of mercantile python package
 The mercantile module provides functions that respectively return the upper
 left corner and bounding longitudes and latitudes for XYZ tiles, spherical
 mercator x and y coordinates, tile containing a given point, and quadkey
 conversion functions for translating between quadkey and tile coordinates.
 .
 Also in mercantile are functions to traverse the tile stack.
 Named tuples are used to represent tiles, coordinates, and bounding boxes.
